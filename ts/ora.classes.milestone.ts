import * as plugins from './ora.plugins';
import { OraProject } from './ora.classes.project';

export interface IOraMilestone {
  id: number;
  title: string;
  created_at: string;
  updated_at: string;
  archived: boolean;
  complete: boolean;
  deadline: string;
  color: string;
  position: number;
}

export class OraMilestone implements IOraMilestone {
  // ======
  // STATIC
  // ======
  public static async getAllMilestonesForProject(oraProjectArg: OraProject) {
    const response = await oraProjectArg.oraOrganizationRef.oraRef.request(`/projects/${oraProjectArg.id}/milestones`, 'GET');
    const milestones: OraMilestone[] = [];
    for (const dataObject of response.data) {
      milestones.push(new OraMilestone(oraProjectArg, dataObject));
    }
    return milestones;
  }

  // ========
  // INSTANCE
  // ========
  public id: number;
  public title: string;
  public created_at: string;
  public updated_at: string;
  public archived: boolean;
  public complete: boolean;
  public deadline: string;
  public color: string;
  public position: number;

  public oraProjectRef: OraProject;
  constructor(oraProjectRef: OraProject, creationObjectArg: IOraMilestone) {
    this.oraProjectRef = oraProjectRef;
    Object.assign(this, creationObjectArg);
  }
}
