import * as plugins from './ora.plugins';
import { OraList } from './ora.classes.list';
import { OraMilestone } from './ora.classes.milestone';

export interface IOraTask {
  milestone_id: number;
  task_type: number;
  creator: number;
  second_id: number;
  title: string;
  description: string;
  last_picture: string;
  last_picture_external_url: string;
  created_at: string;
  updated_at: string;
  deadline: string;
  archived: boolean;
  color: string;
  estimated: string;
  time_tracked: string;
  public: boolean;
  state: string;
  cover_width: number;
  cover_height: number;
  checklist: string;
  comments: number;
  public_comments: number;
  likes: number;
  attachments: number;
  commits: number;
  support_tickets: number;
  value: number;
  points: number;
  points_done: number;
  sprint_id: number;
  position: number;
}

export class OraTask implements IOraTask {
  // ======
  // STATIC
  // ======
  public static async getAllOraTasks(oraListArg: OraList): Promise<OraTask[]> {
    const response = await oraListArg.oraProjectObjectRef.oraOrganizationRef.oraRef.request(
      `/projects/${oraListArg.oraProjectObjectRef.id}/lists/${oraListArg.id}/tasks`,
      'GET'
    );
    const oraTasks: OraTask[] = [];
    for (const dataObject of response.data) {
      oraTasks.push(new OraTask(oraListArg, dataObject));
    }
    return oraTasks;
  }

  // ========
  // INSTANCE
  // ========
  public milestone_id: number;
  public task_type: number;
  public creator: number;
  public second_id: number;
  public title: string;
  public description: string;
  public last_picture: string;
  public last_picture_external_url: string;
  public created_at: string;
  public updated_at: string;
  public deadline: string;
  public archived: boolean;
  public color: string;
  public estimated: string;
  public time_tracked: string;
  public public: boolean;
  public state: string;
  public cover_width: number;
  public cover_height: number;
  public checklist: string;
  public comments: number;
  public public_comments: number;
  public likes: number;
  public attachments: number;
  public commits: number;
  public support_tickets: number;
  public value: number;
  public points: number;
  public points_done: number;
  public sprint_id: number;
  public position: number;

  public oraListRef: OraList;

  constructor(oraListRefArg: OraList, creationObjectArg: IOraTask) {
    this.oraListRef = oraListRefArg;
    Object.assign(this, creationObjectArg);
  }

  public async getMilestone (): Promise<OraMilestone> {
    return (await this.oraListRef.oraProjectObjectRef.getMileStones()).find(milestoneArg => milestoneArg.id === this.milestone_id);
  }
}
