import { expect, tap } from '@pushrocks/tapbundle';
import * as qenv from '@pushrocks/qenv';
const testQenv = new qenv.Qenv('./', './.nogit');

import * as ora from '../ts/index';

let oraInstance: ora.Ora;

tap.test('should create a valid instance for Ora', async () => {
  oraInstance = new ora.Ora(testQenv.getEnvVarOnDemand('ORA_APITOKEN'));
});

tap.test('should get all organizations', async () => {
  const organizations = await oraInstance.getOrganizations();
  expect(organizations.length).to.equal(2);
});

tap.test('should get all projects of an organization', async () => {
  const organizations = await oraInstance.getOrganizations();
  const losslessOrganization = organizations.find(orgArg => {
    return orgArg.name.startsWith('Lossless');
  });
  const projectsInLosslessOrg = await losslessOrganization.getProjects();
});

tap.test('should get all lists for a project', async () => {
  const organizations = await oraInstance.getOrganizations();
  const losslessOrganization = organizations.find(orgArg => {
    return orgArg.name.startsWith('Lossless');
  });
  const projectsInLosslessOrg = await losslessOrganization.getProjects();
  const featureProjects = projectsInLosslessOrg.filter(oraProjectArg => {
    return oraProjectArg.title.includes('Feature');
  });

  console.log('The following Feature Boards are available:');
  featureProjects.forEach(oraProjectArg => console.log(oraProjectArg.title));
  const layerIoProject = featureProjects.find(oraProjectArg =>
    oraProjectArg.title.includes('layer.io')
  );

  const lists = await layerIoProject.getLists();
  console.log('\nThe following lists are available');
  lists.forEach(listArg => console.log(listArg.title));
});

tap.test('should get all tasks for a project', async () => {
  const organizations = await oraInstance.getOrganizations();
  const losslessOrganization = organizations.find(orgArg => {
    return orgArg.name.startsWith('Lossless');
  });
  const projectsInLosslessOrg = await losslessOrganization.getProjects();
  const featureProjects = projectsInLosslessOrg.filter(oraProjectArg => {
    return oraProjectArg.title.includes('Feature');
  });
  const layerIoProject = featureProjects.find(oraProjectArg =>
    oraProjectArg.title.includes('layer.io')
  );
  const lists = await layerIoProject.getLists();
  let tasks: ora.OraTask[] = [];
  for (const list of lists) {
    tasks = tasks.concat(await list.getTasks());
  }
  console.log('the following tasks are available:');
  tasks.forEach(taskArg => console.log(taskArg.title));
});

tap.test('should get a milestone', async () => {
  const organizations = await oraInstance.getOrganizations();
  const losslessOrganization = organizations.find(orgArg => {
    return orgArg.name.startsWith('Lossless');
  });
  const projectsInLosslessOrg = await losslessOrganization.getProjects();
  const featureProjects = projectsInLosslessOrg.filter(oraProjectArg => {
    return oraProjectArg.title.includes('Feature');
  });
  const layerIoProject = featureProjects.find(oraProjectArg =>
    oraProjectArg.title.includes('layer.io')
  );
  const lists = await layerIoProject.getLists();
  let tasks: ora.OraTask[] = [];
  for (const list of lists) {
    tasks = tasks.concat(await list.getTasks());
  }
  console.log('the following tasks are available:');
  const milestone = await tasks[0].getMilestone();
  expect(milestone).to.be.instanceOf(ora.OraMilestone);
  console.log(`Task with title "${tasks[0].title}" is part of milestone "${milestone.title}"`);
});

tap.start();
